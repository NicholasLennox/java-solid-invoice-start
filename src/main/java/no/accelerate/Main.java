package no.accelerate;

public class Main {
    public static void main(String[] args) {
        ElectronicsInvoice invoice = new
                ElectronicsInvoice("Cannon Ultra-print", 100, 2, 0.9);
        invoice.printInvoiceLine();
        invoice.saveToFile();
    }
}
