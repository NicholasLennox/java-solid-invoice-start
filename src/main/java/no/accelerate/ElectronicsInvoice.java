package no.accelerate;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ElectronicsInvoice {
    private Equipment equipment;
    private int quantity;
    private double discount = 1; // Discount: 1 = 100%, 0.8 = 80% (20% discount)

    public ElectronicsInvoice(String description, double price, int quantity,
                              double discount) {
        this.equipment = new Equipment(description,price);
        this.quantity = quantity;
        this.discount = discount;
    }

    private double calculateTotal() {
        return equipment.getPrice() * quantity * discount;
    }

    public void printInvoiceLine() {
        StringBuilder line = new StringBuilder();
        line.append("Description: " + equipment.getDescription());
        line.append(", Price: " + equipment.getPrice());
        line.append(", Quantity: " + quantity);
        line.append(", Discount applied: " + discount);
        line.append(", Discount type: " + getDiscountText());
        line.append(", Total: " + calculateTotal());

        System.out.println(line);
    }

    public void saveToFile() {
        // Build string
        StringBuilder line = new StringBuilder();
        line.append("Description: " + equipment.getDescription());
        line.append(", Price: " + equipment.getPrice());
        line.append(", Quantity: " + quantity);
        line.append(", Discount applied: " + discount);
        line.append(", Discount type: " + getDiscountText());
        line.append(", Total: " + calculateTotal());
        // Save to text file - don't worry about this yet
        String pathToFile = "output.txt";
        try (FileWriter input = new FileWriter(pathToFile);
             BufferedWriter output = new BufferedWriter(input)) {
            // Write data
            output.write(line.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getDiscountText(){
        // Cant switch with double in Java, its weird.
        if(discount == 0.95)
            return "Pensioner";
        if(discount == 0.9)
            return "Student";
        return "Custom";
    }

    // Generated

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
}
